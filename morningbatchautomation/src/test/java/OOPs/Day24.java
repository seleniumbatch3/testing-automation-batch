package OOPs;

public class Day24 {

    /**
     * static Control Flow
     */

    /**
     * Identification of static members from top to bottom
     * Execution of Static variables assignments and static blocks from top to bottom
     * execution of main method
     */
    {

    }
    public static void function2(){
        System.out.println(i);
    }
     static int i =10;

    static String name ;

    public static void function1(){
        System.out.println(name);
        System.out.println(value);
    }


    static
    {
        function1();
       System.out.println ("within in the static block - 1");
    }


    static int value = 100;

    public static void main (String[] test){
        function2();
        function1();
    }

    static
    {
        System.out.println(value);
        System.out.println ("within in the static block - 2");
    }
}
