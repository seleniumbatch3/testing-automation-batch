package OOPs;

public class Parent extends GrandParent{

    public Parent(){
        System.out.println("Parent class default Constructor");
    }
    public Parent(int a){
        System.out.println("Parent class parameterized Constructor");
    }
    public void clickOnNextButton(){
        System.out.println("Click On Next Button ");
    }

    public void pageNotLoadException(){
        System.out.println("Page Not Load Exception");
    }

    public void commonFunction(){

    }
}
