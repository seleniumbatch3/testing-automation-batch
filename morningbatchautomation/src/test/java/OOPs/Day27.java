package OOPs;

public class Day27 {
    /**
     * by default parent default constructor will be called from current class constructor.
     * parent class constructor call must be the first call (super())
     * If current class constructor calling another current class constructor compiler will ignore calling parent
     * class default constructor
     * we can't use super() and this() together explicitly but internally compiler can call super() though we have this()
     * as a first line in the current class constructor
     *
     * Parent class parameterized/default constructor must be called from at least one of the child class constructors
     */

    private static Day27 day27 = null;
    int a;
    String name;
    private Day27(){

    }

    public static Day27 getDay27Instance(){
        if(day27 == null){
            day27 = new Day27();
        }
        return day27;
    }


    public static void main (String[] args){

        Day27 day27 = new Day27();
    }

}
