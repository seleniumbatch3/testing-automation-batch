package OOPs;

import learningjava.Day2;

public class Day26{
    /**
     * Constructor
     * 1. Constructor must be with Class name
     * 2. Follow the Class modifier type
     * 3. Constructors will nto have any return type
     * 4. Always try to have default constructors
     * 5. applied modifiers for constructors - public private protected and default
     * 5. non access modifiers are not allowed
     * 6. Constructors can be overloaded
     * 7. Overriding is not applicable for constructors
     * 8. Constructors can be called from another constructor only not from anywhere else
     * 9. Constructors can be called using this(). this() must be the first statement
     * 10. this key word represents current class object
     * 11.
     *
     *
     */

    String name;
    String college;
    public Day26(){

    }

    public Day26(String name, String college){
        function(name, college);
        this.college = college;
        this.name = name;
        function2();
    }

    public void function(String name, String college){

    }
    public static void function2(){

    }

    public static void main (String[] args){

        Day26 day26 = new Day26("Siva", "SSN");
        System.out.println(day26.name);
        System.out.println(day26.college);

        Day26 day261 = new Day26("Harsha", "Andhra");

        System.out.println(day261.name);
        System.out.println(day261.college);
        Day27 day27 = Day27.getDay27Instance();

    }



}
