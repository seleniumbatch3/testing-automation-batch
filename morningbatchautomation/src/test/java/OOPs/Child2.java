package OOPs;

public class Child2 extends Day22{


   {
        System.out.println("Final Method");
    }

    public static void function1(int a, String name){
        System.out.println("Static Child method");
    }


   public String function1(String name,int age){
        System.out.println("Child method");
        return "";
    }

    public static void main (String... args){
        Child2 child2 = new Child2();
        child2.function1(10, "Murthy");
    }


}
