package OOPs;

public class Day19 {
    /**
     * Data Hiding
     * Abstraction
     */

    public String name;
    public int employeeID;

    private int salary;
    private int aadharNumber;
    private int panCardNumber;

    public Day19(){

    }

    public Day19(String name, int employeeID){
        this.name = name;
        this.employeeID = employeeID;
    }

    public void setSalary(int salary){
        this.salary = salary;
    }

    public int getSalary(){
        return salary;
    }

    public void setAadharNumber(int aadharNumber){
        this.aadharNumber = aadharNumber;
    }

    public int getAadharNumber(){
        return aadharNumber;
    }

    public void setPanCardNumber(int panCardNumber){
        this.panCardNumber = panCardNumber;
    }

    public int getPanCardNumber(){
        return panCardNumber;
    }

    public void settingEmployeeValues(){
        setSalary(10000);
        setAadharNumber(123456789);
        setPanCardNumber(345678);
    }

    public static void main (String[] value){
        Day19 day19 = new Day19();
        day19.settingEmployeeValues();

        System.out.println(day19.getSalary());
    }

}
