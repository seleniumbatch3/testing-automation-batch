package OOPs;

import learningjava.day5.Day16;

public class Day20 extends Parent {

    /**
     * Method Loading
     * static polymrophism or compile time polyMorphsim or Over loading
     */


    public void function1(){

        System.out.println("Empty function");
    }

    public static void function1(int value){

        System.out.println(value*value);
    }

    public static String function1(String name){

        return name;
    }

    public static void function1(int val, int age){



        int a=10;


        System.out.println(val* age);
    }


    public static void main(){

    }

    @Deprecated
    public int addNumbers(int a){
        return a*a;
    }

    public int addNumbers(int a, int b){
        return a*b;
    }

    int function2(int value){
        System.out.println("Input value is Integer");
        return value;
    }

    int function2(byte value){
        System.out.println("Input value is byte");
        return value;
    }

    double function2(double value){
        System.out.println("Input value is byte");
        return value;
    }

    String function3(String value){
        String name = null;
        return value;
    }

    Day20 function3(Day20 value){
        StringBuffer name = null;
        return value;
    }

    @Override
    public void commonFunction() {
        System.out.println("Gayathir Implementation");
    }

    public static void main (String[] test){
        Day20 day20 = null;
        day20 = new Day20();
        day20.function1();

        day20.function2(5);

        day20.function3("Murthy");
    }
}

