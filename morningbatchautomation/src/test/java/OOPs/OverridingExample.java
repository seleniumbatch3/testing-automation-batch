package OOPs;

import learningjava.Day2;

public class OverridingExample {

    public Parent parent;

    public void function(String name){
        switch (name){
            case "Gayathri":
                parent = new Day20();
                parent.commonFunction();
                break;
            case "Venkat":
                parent = new Day23();
                parent.commonFunction();
                break;
            case "Supraja":
                parent = new Day21();
                parent.commonFunction();
                break;
        }

    }

    public void function1(String name){
        switch (name){
            case "Gayathri":
                Day20 day20 = new Day20();
                day20.commonFunction();
                break;
            case "Venkat":
                Day23 day23 = new Day23();
                day23.commonFunction();
                break;
            case "Supraja":
                Day21 day21 = new Day21();
                day21.commonFunction();
                break;
        }

    }

    public static void main (String... args){
        OverridingExample overridingExample = new OverridingExample();
        overridingExample.function("Venkat");

    }
}
