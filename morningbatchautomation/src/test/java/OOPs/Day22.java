package OOPs;

public class Day22 extends Day19 {
    /**
     * https://www.scientecheasy.com/2019/03/method-overloading-interview-programs.html/
     * @param a
     */

   final static public void function1(int a){

       System.out.println("Final Method");

    }

    public void function1(int a, int b){

    }

    public void function1(byte a){

    }

    public static void function1(int a, String name){
        System.out.println("Static Parent Method");
    }

    public Object function1(String name,int age){

       return "";
    }

    public void getName(Day19 name){
       name = null;

    }

    public void getName (Day22 Test){
       Test = null;
    }


    public void getName1(Object name){
        name = null;

    }


    public void getAddress(){

    }
    public String name(){
        System.out.println("murthy");
        return "Venkat";
    }

    public static void main(String... arg){

       Day22 day22 = new Day22();

       day22.getName1("Murthy");
       Day19 day19 = new Day22();

    }

}
