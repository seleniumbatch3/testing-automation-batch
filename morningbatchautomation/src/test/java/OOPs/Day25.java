package OOPs;

public class Day25 {

    /**
     * Instance Flow
     *  by default static flow will be executed and in the process of the static flow
     *  if we have any object creation, the flow of instance members is as fallow
     *
     *  1. identify all instance variables/members
     *  2. execute blocks and assign variables from top to bottom
     *  3. Execute Constructor
     */

    {
        function1();
        function2();
        System.out.println("This is first block");
    }

    int i;
    int j;

    public Day25(){

    }

    Day25(int i, int j){
       this.i = i;
       this.j = j;

    }
    static
    {
        System.out.println("This is first static block");

    }

    public void function1(){
        System.out.println(a);
        System.out.println("value of i " + i);
        System.out.println("This is first function");
    }
    int a = 100;
    static int b;
    public static void function2(){
        System.out.println(b);
        System.out.println("This is first function");
        System.out.println(name);
    }
    public static void main (String... args){
        System.out.println("This is main method");
        Day25 day25 = new Day25();
        System.out.println("This is after main method");
        day25.function1();
        Day25 day251 = new Day25();
        day251.a=20;
        day251.function1();

    }

    static
    {
        System.out.println("This is 2nd static block");
    }

    static String name = "Venkat";

}
