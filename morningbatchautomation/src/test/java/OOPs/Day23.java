package OOPs;

public class Day23 extends Parent{

    /**
     * Multiple or Multilevel inheritence
     */

    public void functionFromDay23(){
        launchBrowser();
        pageNotLoadException();
        clickOnNextButton();
    }


    void function(GrandParent p){
        System.out.println(p);
    }

    void function(Parent p){
        System.out.println(p);
    }
    //OOPs.Parent@1b6d3586

    //OOPs.GrandParent@1b6d3586

    public static void main (String... args){
        Day23 day23 = new Day23();
        day23.functionFromDay23();
        char v = 44444;

        Day23 day231 = new Day23();

        Parent p = new Day23();

        day23.function(new Day23());


    }

    @Override
    public void commonFunction() {
       System.out.println("VENKATA RAMANA MURTHY IMPLEMENTATION");
    }
}
