package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.session.FirefoxFilter;
import org.testng.annotations.Test;

public class Day4 {

    ChromeDriver chromeDriver = null;
    FirefoxDriver firefoxDriver = null;
    InternetExplorerDriver internetExplorerDriver= null;

    RemoteWebDriver remoteWebDriver = null;
    WebDriver webDriver = null;

    public void function(){

        WebDriver webDriver = new ChromeDriver();

        RemoteWebDriver remoteWebDriver = new ChromeDriver();

        ChromeDriver chromeDriver = new ChromeDriver();

        FirefoxDriver firefoxDriver = new FirefoxDriver();
    }


    public ChromeDriver launchChromeBrowser(){
        System.setProperty("webdriver.chrome.driver",
                "C:\\Automation Batches\\Evening  9 30 AM\\" +
                        "testing-automation-batch\\morningbatchautomation\\drivers\\chromedriver.exe");

        chromeDriver=  new ChromeDriver();
        return chromeDriver;
    }

    public FirefoxDriver launchFireFoxBrowser(){
        System.setProperty("webdriver.chrome.driver",
                "C:\\Automation Batches\\Evening  9 30 AM\\" +
                        "testing-automation-batch\\morningbatchautomation\\drivers\\chromedriver.exe");

        firefoxDriver=  new FirefoxDriver();
        return firefoxDriver;
    }


    public void launchApplication(){
        launchChromeBrowser().get("");
    }
    public void launchApplicationonFire(){
        launchFireFoxBrowser().get("");
    }

    public WebDriver launchBrowsers(String browserType){
        switch(browserType){
            case "chrome":
                webDriver = new ChromeDriver();
                break;
            case "firefox":
                webDriver = new FirefoxDriver();
                break;
            case "ie":
                webDriver = new InternetExplorerDriver();
                break;

        }
        return webDriver;
    }

    public void enterCredentials(String browserType){
        launchBrowsers(browserType).get("");


    }

    @Test
    public void test(){
        enterCredentials("firefox");
    }
}
