package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class Day2 {

    /**
     * Types of WebDrivers in Selenium
     * WebDriver - interface
     * RemoteWebDriver - Classes
     * EventFiringWebDriver - Classes
     *
     */
    {
        WebDriver webDriver = null;
        RemoteWebDriver remoteWebDriver;
        EventFiringWebDriver eventFiringWebDriver;

        WebElement webElement;

        webDriver.get("");
        webDriver.getTitle();
        webDriver.getPageSource();
        webDriver.getCurrentUrl();
        webDriver.close();
        webDriver.quit();
//        webDriver.findElement();
//        webDriver.findElements();
//        webDriver.navigate().to();
        webDriver.getWindowHandle();
        webDriver.getWindowHandles();
        webDriver.switchTo();
        webDriver.manage().window().maximize();

    }

}
