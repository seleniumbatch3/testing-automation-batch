package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Day1 {
    /**
     * Selenium Architecture
     * 1. Selenium client library
     */

    public void launchBrowser(){
//        webDriver.get("");
//        webDriver.getTitle();
//        webDriver.getPageSource();
//        webDriver.getCurrentUrl();
//        webDriver.close();
//          webDriver.getWindowHandle();


        System.setProperty("webdriver.chrome.driver", "C:\\Automation Batches\\Evening  9 30 AM\\testing-automation-batch\\morningbatchautomation\\drivers\\chromedriver.exe");

        ChromeDriver chromeDriver = new ChromeDriver();
        chromeDriver.get("https://www.facebook.com");

        String title = chromeDriver.getTitle();
        System.out.println("Browser Title " + title);

        String pageSource = chromeDriver.getPageSource();
        System.out.println("Browser Title " + pageSource);


        String currentURL = chromeDriver.getCurrentUrl();
        System.out.println("Browser Title " + currentURL);

        String windowID = chromeDriver.getWindowHandle();
        System.out.println("Browser Title " + windowID);

         chromeDriver.manage().window().maximize();;
        System.out.println("Browser Closed");



    }

    public static void main(String[] test){
        Day1 day1 = new Day1();
        day1.launchBrowser();
    }
}
