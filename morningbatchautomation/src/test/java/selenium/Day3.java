package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Day3 {

    /**
     * Element Identification
     *
     * ID
     * name
     * className
     * tag
     * linkText
     * PartialLInkText
     * xpath
     * cssSelector
     *
     */
   static WebDriver webDriver;

    public void launchBrowser(){

        System.setProperty("webdriver.chrome.driver", "C:\\Automation Batches\\Evening  9 30 AM\\testing-automation-batch\\morningbatchautomation\\drivers\\chromedriver.exe");

        ChromeDriver chromeDriver = new ChromeDriver();
        chromeDriver.get("https://www.facebook.com");
        WebDriverWait webDriverWait = new WebDriverWait(chromeDriver, 5);
        webDriverWait.until(ExpectedConditions.alertIsPresent());
//        WebElement emailEditBox = chromeDriver.findElement(By.id("email"));
        chromeDriver.findElement(By.xpath("//a[text() ='Create New Account']")).click();
//        List<WebElement> allLinks = chromeDriver.findElements(By.linkText("a"));
//        for ( WebElement link: allLinks){
//            System.out.println(link.getText());
//        }
        String text = chromeDriver.findElement(By.xpath("//div[@id='reg_form_box']/div[7]/div/a")).getText();
//
//        emailEditBox.sendKeys(new StringBuffer("Testng"));
        System.out.println(text);


    }

    public static void main(String... test){

        Day3 day3 = new Day3();
        day3.launchBrowser();

    }

}
