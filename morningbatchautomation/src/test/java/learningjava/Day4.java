package learningjava;

public class Day4 {

    /**
     * Literals
     */


    /**
     * Types of Literals
     * 1. Decimal
     * 2. Octa Form
     * 3. Hexa Decimal Form
     * 4. Binary Form
     */

    /**
     * Decimal Form which is in readable format
     */
    static int a = 34567;

    int l = 6_7_8;
    static long mobileNumber = 404_7290_735l;

    /**
     * Octa Form
     * it accepts 0-7
     */

    static int b = 01111;


    /**
     * Hexa Decimal Form
     * 0-9 Numbers and it accpts A-F also
     * A= 10
     * B =11
     * C=12
     * D=13
     * E = 14
     * F =15
     */

    static long y = 0XAB234BC567DEFl;
    byte h = 0xA;
    short g = 0XF;

    static float z = 0X100A;


    /**
     * Binary Form
     * 0 and 1
     * 0B10101010101010
     *
     */

    /**
     * char data type
     *  a-z
     *  A-Z
     *  0-9
     */

    static char ch = 0X754F;

    public static void main (String... test){

        byte b = 0000040;
        short s =061111;
        long l = 03456576567456577l;
        System.out.println(mobileNumber);
        boolean  x = false;

    }
}
