package learningjava.day5;

public class Day7 {

    /**
     * instance block
     * static block
     */
    Day7(){

    }

    int a;
    static int b ;

    {
        a= 100;
    }

    static
    {
        b = 100;
    }



    void add(double d){
        System.out.println (d*d);
        add(10);

    }
    static void add(int a){
        System.out.println(a*a);
    }

    /**
     * String Concatenation
     */

    public static void main (String[] args){
        Day7.add(b);


        System.out.println("Supraja"+ 10+20);
        System.out.println(10+"Supraja"+20);
        System.out.println(10+20+"Supraja");
    }

}
