package learningjava.day5;

public class Day6 {

    /**
     * Variables
     *  1. primitive Data Types
     *  2. Non - Primitive DAta Types
     */
    /**
     * Types of Variable
     * 1. instance variables
     * 2, static variables
     * 3. local variables
     */

    /**
     * Instance variable
     */
    int age;
    double d;

    /**
     * Static Variables
     */
    static int money = 200;
    static double price ;


    /**
     * Constructor
     * 1. default constructor
     * 2. parameterized constructor
     */

    Day6(){

        System.out.println("My Default Constructor");

    }

    Day6(int age, double d){
       this.age = age;
       this.d =d;
    }


    /**
     * functions
     *  1. non parameter function
     *  2. Parameterized functions
     *
     *  a. static function
     *  b. non-static function
     */


    void addition(){
        age = 40;
        int salary = 100;
        System.out.println(age);
        System.out.println(money);
    }

    static void subtraction (){
       int salary = 100;
       salary = 200;
        System.out.println(salary);
    }

    /**
     * Examples for return type
     */

    int addingTwoIntegers(){
        int a= 100;
        int b = 200;
        int c = a+b;
        return c;

    }

    void displayMessage(){
        System.out.println("Display value");
    }

    /**
     * With parameters
     */

    int subtraction(int a, int b){
        int c = a-b;
        return c;
    }

    /**
     * blocks
     *  1. static block
     *  2. instant block/non-static blocl
     */

    /**
     * main method
     */


    public static void main (String[] test){
      Day6 obj = new Day6();
      Day6 obj1 = new Day6(50, 100);

      System.out.println("Values for obj " + obj.age);
     System.out.println("Values for obj1" + obj1.age);

     Day14 day14 = new Day14();
        System.out.println(day14.function4());

    }
}
