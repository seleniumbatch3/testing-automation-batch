package learningjava.day5;

public class MyFirstClass {

    int employID;
    String name ;
    String gender;
    int age;
    double salary;
    String reportingManager;
    String location;

    MyFirstClass(int employID){
        this.employID = employID;
    }

    public String verifyManOrWoman(String gender, int age){

        if (gender.equalsIgnoreCase("male") && age > 18){
            return "This person is a an Adult Man";
        }
        else if (gender.equalsIgnoreCase("female") && age >18){
            return "This person is a an Adult Woman";
        }
        else {
            return "This person is minor";
        }

    }

    public String verifyEmployeeDetails(int searchEmployeeID){
        if (this.employID == searchEmployeeID){
            this.name = "Gayathri";
            this.age = 25;
            this.salary = 25000;
            this.reportingManager = "XYZ Person";
            this.location = "Hyderabad";
            return "Name of "+ employID + " is " + name + " Age : " + age
                    + "Salary " + salary + " Reporting Manage " + reportingManager + " Location " + location;
        }
        return "NO employe Match is found";
    }

    public static void main (String...test){

        MyFirstClass obj = new MyFirstClass(455555);
        System.out.println ("value for Object1 " + obj.verifyEmployeeDetails(455555));

    }

}
