package learningjava.day5;

public class Day15 {
    /**
     * protected
     * final
     * static
     * abstract
     */
  public Day15(){

    }

    public static String function1(){
        return "Venkata Ramana Murthy";
    }

    public String function2(){
        return "Venkata Ramana Murthy";
    }

    public static final void function3(){

    }

    private static final void function4(){

    }

    protected static final void function5(){

    }
    static final void function6(){

    }
}
