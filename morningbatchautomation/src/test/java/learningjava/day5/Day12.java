package learningjava.day5;

import java.util.*;

public class Day12 {
    /**
     * For
     * For -each
     * while
     * Do - While
     */

    int a=100, b=200;

    void function1(){

        ArrayList<Boolean> names = new ArrayList<>();
        names.add(false);
        names.add(true);
        names.add(false);

        List<String> collegeName = new ArrayList<>();
        collegeName.add("AKVK");
        collegeName.add("SSN");
        collegeName.add("VVS");

//        for (int a = 0, j=0 ;a<names.size(); ++a, j++ ){
//            System.out.println("Names" + names.get(a) + " === " + "collegeName" + collegeName.get(j));
//        }

        for (boolean value : names){
            System.out.println("names " + value);

        }

    }

    void function2(){



        Set<String> names = new HashSet<>();
        names.add("MURTHY");
        names.add("Harsha");
        names.add("Navya");

        Iterator value = names.iterator();
        int a = 200;
        int b = 200;
        while (a == b){

            System.out.println("names ");
        }
        for (String values : names){
            System.out.println("names " + values);
        }

    }


    public static void main (String... test){

        Day12 day12 = new Day12();
        day12.function2();
    }
}
