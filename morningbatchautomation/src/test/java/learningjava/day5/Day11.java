package learningjava.day5;

public class Day11 {


    /**
     * Selection Statement
     *  if -else
     *  switch - case
     */

   int a = 100;
    int b = 200;
    String name = "Venkata Ramana Murthy";

    void function1(){
        if (a < b){
            System.out.println ("Print value of A" + a);
        }
        else if (a==100){
            System.out.println ("Print value of B" + b);
        }
        else if (a>b){

        }
        else {

        }

    }

    void function2(){

        if (a<b)
            System.out.println ("Print value of a" + a);
        System.out.println ("Print value of B" + b);

    }


    void function3(){
        if (a>b);

        if (a==b){
            System.out.println ("Print value of B" + b);
          name = "Gayathri";
        }
        else if (a==b){
            name = "Supraja";
        }

        int c = (a==b)? 5:

                (a>b)? 3:2;

    }


    void function4(){

        switch(a){
            case 200:
                name = "Gayathri";
                break;
            case 5:
                name = "Supraja";
                break;
            case 44444444:
                name = "Siva";
                break;
            default:
                name = "Venkat";
        }

        System.out.println(name);
    }

    void function5(String name ){

        switch (name){
            case "Monday":
            case "Tuesday" :
            case "Wednesday":
            case "Thursday":
                System.out.println(name + " is WeekDay");
                break;
            default:
                System.out.println(name + " is Weekend");
        }

    }

    public static void main (String... args){
        Day11 day11 = new Day11();
        day11.function5("Friday");
    }
}
