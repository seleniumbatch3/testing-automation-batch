package learningjava.day5;

import learningjava.Day1;

public class Day17 {
    /**
     * Arrays Continuation
     */

    void function1(){

        int[] a = new int[3];
        a[0] = 10;
        a[1] = 20;
        a[2] = 30;

        int[] value = {10, 20, 30};
    }

    /**
     * Two Dimensional Array
     */

    static void function2(){
        int[][] value = new int[2][6];

        int[][] value2 = new int[4][];

        int[][][] value3 = new int[4][2][5];

//        {{1,2,3,4,5}, {3,4,5,6}, {4}, {1,2,3}}


        value[0][0] = 10;
        value[0][1] = 20;

        value[1][0] = 30;
        value[1][1] = 40;

        int[][] value1 = {{10, 20, 30, 40}, {20, 30, 50, 60}, {5,3}, {1}};

        for ( int[] i : value1){

            for( int a : i){

                System.out.println("Values of A " + a);
            }
        }
    }
    public static void main(String[] murthy) {
        function2();

    }
}
