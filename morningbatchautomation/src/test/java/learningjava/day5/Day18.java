package learningjava.day5;

import OOPs.Day19;
import learningjava.Day1;

public class Day18 {
    /**
     * Types of Variables
     */
    String name ="murthy";
    int rollNumber = 0000;

    static String collegeName ;


    public Day18(){

    }
    public Day18(String name, int rollNumber){
        this.name = name;
        this.rollNumber = rollNumber;
    }


    public static void main(String[] value){

        Day18 day18 = new Day18("Murthy", 3456);
        Day18 day19 = new Day18("Supraja", 45676);

        Day18 day20 = new Day18();
        day20.rollNumber = 1234;
        day20.name = "Siva";

        Day18 day21 = new Day18();
        day21.name = "Gayathri";


        day21.collegeName = "SSNDegreeCollege";

        System.out.println(" Name of Student is === " + day18.name + " rollNumber is === " + day21.rollNumber);
//        System.out.println("College Name === " + collegeName);

        System.out.println("College Name === " + day18.collegeName);

        Day19 day191 = new Day19();

        day191.settingEmployeeValues();

        int salary = day191.getSalary();
        int panCardNumber = day191.getPanCardNumber();
        System.out.println("PanNumber ==== " + panCardNumber);
    }
}
