package learningjava.day5;

import OOPs.Day20;
import learningjava.Day2;

public class Day10 {

    /**
     * Assignment Operators
     * 1. Simple
     * 2. Chained
     * 3. Compound
     */

    /**
     * Compound Operators
     * +=
     * -=
     * *=
     * %=
     * /=
     */





    static int  addition(){

        int a,b, c, d;
        a=20;
        b =30;
        c=40;
        d=50;

//        return  a +=b -=c *=d /=2;

        return a/=-200;

//        int a= 10;
//        a +=20;
//        return a;
    }

    static int subtraction (){
        int a= 10;
        a -=20;
        return a;
    }

    static int multiplication (){
        int a= 10;
        a *=20;
        return a;
    }
    static int bitWiseOperators(){
        int a =15;
        int b =10;
        int c = a^b;
        return c;
    }
    public static void main (String... test){
        System.out.println (bitWiseOperators());

        Day15 day15 = new Day15();
        day15.function1();

        Day20 day20 = new Day20();
        day20.addNumbers(10);
    }

    String ternaryOperator(){
        int a = 100;
        int b = 200;

        String value = "TEsting";

        return  (a<b)
                ?

                "Venkat"
                :
                "GAyathri";

    }

    String ternaryOperator1(){
        int a = 100;
        int b = 200;

        if (a<b){
            String value = "TEsting";
            value = value+value;
            return "venkat";
        }
        else{
            return "Gayathri";
        }

    }
    /**
     * Ternary Operator/conditional operators
     * ?
     * :
     */
}
