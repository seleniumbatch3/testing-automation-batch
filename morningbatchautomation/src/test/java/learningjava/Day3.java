package learningjava;

import learningjava.day5.Day14;
import learningjava.day5.Day15;

public class Day3 {

    /**
     * Data Types
     *
     * byte
     * short
     * int
     * long
     *
     * float
     * double
     *
     * boolean
     * char
     */

    /**
     * byte
     */

   static int a ;
   static  byte b;
   static  short s;
    static long l;
    static float f;
    static double d;

    static boolean x;
    static char c;

    static void defaultValuesOfPrimitiveDataTypes(){
        System.out.println("default value of int " + a);
        System.out.println("default value of byte " + b);
        System.out.println("default value of short " +s);
        System.out.println("default value of long " +l);
        System.out.println("default value of float " +f);
        System.out.println("default value of double " +d);
        System.out.println("default value of booelan " + x);
        System.out.println("default value of char " + c);
    }


    public static boolean isDisplayed(String input){
       return input.equalsIgnoreCase("Murthy");
    }

    public static void main (String... arg){

        Day3 day3 = new Day3();
       Day15 day15 = new Day15();
       day15.function1();



        defaultValuesOfPrimitiveDataTypes();
        System.out.println(isDisplayed("Gayathri"));

        byte b = -128;

        /**
         * short is 2 byte and 16 bits
         */

        short s = 32767;

        /**
         * int datatype is 4 byte 32 bit
         * -2,147,483,648 to 2,147,483, 647
         */
        int a = 215678804;

        /**
         * LONG IS 8 byte and 64 bit
         * 	-9,223,372,036,854,775,808
         * 9,223,372,036,854,775,807
         *
         */

        long l = 9223372036854775807L;


        /**
         * Float point data types
         * float
         * double
         */

        double d = 24534534.2423455454353;

        double e = 24249504.00;


        /**
         * boolean data type
         * true or false
         */

        boolean x = true;

        /**
         * char
         *
         * 0-65535
         */
        char ch  = 'a';
        char cha = 65;

    }

}
