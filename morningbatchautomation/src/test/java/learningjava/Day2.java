package learningjava;

import learningjava.day5.Day15;

public class Day2  {

    public Day2(){

    }
    {

    }
   public String function2(){
        Day15.function1();
        return "Supraja";
    }


    int testingMyCode1;
    int testingMyCode2;
    int var1;
    int var2;
    int var3;

    int testing_My_Code;
    int testing_value$;

    public void _testingMyApplication(){
        function2();

    }

/**
 * Key words for DataType
 *
 *      byte
 *     short
 *     int
 *     long
 *     float
 *     double
 *     boolean
 *     char
 *
 */

/**
 * Key words for control-flow
 *         if
 *         else
 *         switch
 *         case
 *          default
 *          while
 *         do
 *         for
 *         break
 *         continue
 *         return
 *
 */


/**
 * Key words for modifiers
 * ---------------------------
 *     public
 *     private
 *     protected
 *     static
 *     final
 *     abstract
 *     synchronized
 *     native
 *     strictfp
 *     transient
 *     volatile
 */

    /**
     * key words for Exceptions
     * ----------------------------
     *         try
     *         catch
     *         finally
     *         throw
     *         throws
     *         assert
     */

/**
 * Class/Interface related key word
 * ----------------------------------
 *          class
 *          interface
 *          extends
 *         implements
 *         package
 *         import
 *
 *         6. Object related key words
 * -----------------------------
 *         new
 *         instanceof
 *         this
 *         super
 */

    /**
     * return type
     * -----------------------------
     *     void
     */

    /**
     * unused keywords
     * ----------------------------
     *         goto
     *         const
     */

/**
 * key word for literals
 * ---------------------------
 *         true
 *         false
 *         null
 *
 *         enum
 */


/**
 * package - it should be single word with lower case
 * class/Interface - TestingJavaClass- maximum 2 to 3 words, Dont delare Class with any Number (in the begining or ending)
 * Class names will not accept any special character
 * variable - variableDeclarationDone - > it accepts or aloow numbers and spcial character (_ and $)
 * testingSomething_1;
 * function/method
 * constructor
 * block
 */


}
